﻿<br>

![pp](cv/cara.png)
# Gerard Torredefló Fargas

## Dades
**Data de naixement:** 7/8/2001 <br>
**Nacionalitat:** Espanyola<br>
**Correu electrònic:** gerard.torredeflo.7e5@itb.cat

## Educació
| Centre | Estudis |
| ------ | ----------- |
|  Escola Jesuïtes el Clot  | Sistemes Microinformàtics i Xarxes <br> 2017 - 2019 |
|  Escola Jesuïtes el Clot  | Desenvolupament d'Aplicacions Web (1r)<br> 2019 - 2020 |
|  Institut Tecnològic de Barcelona  | Desenvolupament d'Aplicacions Multiplataforma: Videojocs (2n) <br> 2021 - Present |

## Coneixements
- Programari ofimàtic
  - Microsoft Office
  - Google Workspace
- Llenguatges de marques
  - HTML
  - CSS
  - XML
  - XSD
- Llenguatges de programació
  - C
  - Python
- Gestió i manteniment de sistemes
## Experiència
| Escola Jesuïtes Kostka | Tècnic Informàtic <br> Juny 2018 - Desembre 2018 |
| ------ | ----------- |


## Aficions
- Lectura
- Cinema
- Videojocs
- Música